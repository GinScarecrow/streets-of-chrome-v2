﻿using UnityEngine;
using System.Collections;

public class FighterAnimation : MonoBehaviour
{

    public Animator animator;
    public GameObject fighter;
    public float movementSpeed;

    private Transform defaultCamTransform;
    private Vector3 resetPos;
    private Quaternion resetRot;
    private GameObject cam;


    void Start()
    {
        cam = GameObject.FindWithTag("MainCamera");
        defaultCamTransform = cam.transform;
        resetPos = defaultCamTransform.position;
        resetRot = defaultCamTransform.rotation;
        fighter.transform.position = new Vector3(0, 0, 0);
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.D))
        {

            animator.SetBool("Movement", true);
            fighter.transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);


        }
        else if (Input.GetKey(KeyCode.A))
        {
            animator.SetBool("Movement", true);
            fighter.transform.Translate(Vector3.back * movementSpeed * Time.deltaTime);
        }
        else
        {
            animator.SetBool("Movement", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("Attack1");

        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            animator.SetTrigger("Attack 2");

        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            animator.SetTrigger("Attack3");

        }
    }
}