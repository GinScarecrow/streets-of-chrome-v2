﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecentScore : MonoBehaviour
{
    private float timeLeft;

    // Start is called before the first frame update
    void Start()
    {
        timeLeft = PlayerPrefs.GetFloat("Player Score");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnGUI()
    {
       GUI.Label(new Rect(800, 300, 100, 200), "SCORE: " + timeLeft * 100);
    }
}
