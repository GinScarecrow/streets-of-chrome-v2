﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnd : MonoBehaviour
{
    private float timeLeft = 30.0f; 
     

    private void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
            GameOver();
    
    }
    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 20), "" + timeLeft);
        PlayerPrefs.SetFloat("Player Score", timeLeft);
    }
    private void GameOver()
    {
        SceneManager.LoadScene(0); 
    }

}
