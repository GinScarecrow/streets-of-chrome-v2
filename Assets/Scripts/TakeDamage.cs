﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour
    {
   
    // Start is called before the first frame update
    void Start()
    {

    }

    public bool Death = false;

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Object Entered the trigger");
       // print("hi");
        if(collision.collider.gameObject.tag == "Player")
        {
            Debug.Log("Object was a player");
            GetComponent<Animator>().SetTrigger("Death");
        }
        
        if(!GetComponent<Animation>().isPlaying && Death == true)
        {
            Destroy(gameObject);
        }
        
        
    }


    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Object Entered the trigger");
        GetComponent<Animator>().SetTrigger("Death");
        { 
        if (!GetComponent<Animation>().isPlaying && Death == true)
        {
            Destroy(gameObject);
        }
        }
    }
   
// Update is called once per frame
void Update()
    {
        
    }
    
}
